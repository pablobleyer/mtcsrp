#
# MTCSRP Makefile
#

MBEDTLS = c:/Work/zhk/lib/mbedtls
CLIB = c:/Work/zhk/bld/x86

CINC = $(MBEDTLS)/include/polarssl . # $(MBEDTLS)/configs

COBJ = mtcsrp csrp_win

INCLUDES = $(addprefix -I,$(CINC))
LIBPATH = $(addprefix -L,$(CLIB))

ifeq ($(BUILD), msvc)

TOOLDIR = C:/VisualStudio/VC
TOOLBIN = $(TOOLDIR)/bin/

# dumpbin executable
DUMPBIN = $(TOOLBIN)dumpbin
# lib executable
VCLIB = $(TOOLBIN)lib

CC = $(TOOLBIN)cl

else

# TOOLDIR = C:/MinGW64/mingw64
TOOLDIR = C:/MinGW
TOOLBIN = $(TOOLDIR)/bin/

CFLAGS = -std=c11 -g -O3 -Wall $(INCLUDES) # -DSRAND_ZERO -DDEBUG # -Wl,--kill-at
LDLIBS = $(LIBPATH) -lmbedtls

# pexports executable (Note: GCC must be in the executables path)
PEXPORTS = $(TOOLBIN)pexports
# dlltool executable
DLLTOOL = $(TOOLBIN)dlltool

CC = $(TOOLBIN)gcc

OBJ =  $(addsuffix .o,$(COBJ))

endif

.PHONY: clean distclean

all: mtcsrp_test

# G++ needed @x function suffix to link executable, thus header file is provided and later removed with dlltool
#libmtcsrp.a:
#	$(PEXPORTS) -h $mtcsrp.h $*.dll > $*.def
#	$(DLLTOOL) -k -d $*.def -l $@

libmtcsrp.a: libmtcsrp.a($(OBJ))

mtcsrp.dll: $(OBJ)
	$(CC) -shared $^ -o $@ $(LDLIBS) # -Wl,--out-implib,lib$*.a

mtcsrp.lib:
#	$(DUMPBIN) /exports $*.dll > $*.def
	$(PEXPORTS) $*.dll > $*.def
	$(VCLIB) /machine:x64 /def:$*.def /OUT:$@

mtcsrp_test: mtcsrp.o csrp_win.o

clean:
	$(RM) *.o *.def *.a *.exe *.lib

distclean: clean
	$(RM) *.bak
