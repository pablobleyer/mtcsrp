/** @file
	Custom random number generator initialization
	@author Pablo Bleyer <pablo@gozendo.com>
*/

/*
 * Secure Remote Password 6a implementation
 * Original copyright:
 * Copyright (c) 2010 Tom Cocagne. All rights reserved.
 * https://github.com/cocagne/csrp
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Zendo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <Windows.h>
#include <Wincrypt.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <stdint.h>
#include <stdbool.h>

#define BUFFER_SIZE 4 // 64

bool
srp_random_init()
{
	HCRYPTPROV wctx;
	uint8_t buff[BUFFER_SIZE];

	bool r = CryptAcquireContext(&wctx, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT) == TRUE
		&& CryptGenRandom(wctx, sizeof(buff), (BYTE*) buff) == TRUE
		&& CryptReleaseContext(wctx, 0) == TRUE;

	if (r)
	{
#ifdef SRAND_ZERO
		srand(0); // RAND_seed(buff, sizeof(buff));
#else
		srand(*(int *)buff); // RAND_seed(buff, sizeof(buff));
#endif
	}

	return r;
}
