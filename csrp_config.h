/** @file
	CSRP configuration file
*/

#ifndef CSRP_CONFIG_H
#define CSRP_CONFIG_H

// Prime-generators used by the library
// Define them to include the specific Ng pair support in the code
#define SRP_NG_1024
#define SRP_NG_1536
#define SRP_NG_2048
#define SRP_NG_3072 // 2^3072 - 2^3008 - 1 + 2^64 * ([2^2942 pi] + 1690314)
#define SRP_NG_4096 // 2^4096 - 2^4032 - 1 + 2^64 * ([2^3966 pi] + 240904)
#define SRP_NG_6144 // 2^6144 - 2^6080 - 1 + 2^64 * ([2^6014 pi] + 929484)
#define SRP_NG_8192 // 2^8192 - 2^8128 - 1 + 2^64 * ([2^8062 pi] + 4743158)

#endif // CSRP_CONFIG_H
