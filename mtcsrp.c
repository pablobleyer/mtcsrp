/** @file
	Customization from csrp srp.c to use mbed TLS
	@author Pablo Bleyer <pablo@gozendo.com>
*/

/*
 * Secure Remote Password 6a implementation
 * Original copyright:
 * Copyright (c) 2010 Tom Cocagne. All rights reserved.
 * https://github.com/cocagne/csrp
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Zendo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#if defined(_WIN32)
	#include <Windows.h>
	#include <Wincrypt.h>
#elif defined(__CC_ARM)
#else
	#include <sys/time.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>

#include "mtcsrp.h"

/* Flag that the random number generator has been initialized. */
static bool g_initialized = false;

#define MPI_OK 0
#define CRYPT_OK 0
#define CRYPT_INVALID_HASH (-1)

#define mpi_iszero(a) ((a)->n == 0)
#define mpi_iseven(a) ((a)->n > 0 && (((a)->p[0] & 1) == 0))
#define mpi_isodd(a)  ((a)->n > 0 && (((a)->p[0] & 1) == 1))

typedef union
{
	sha1_context sha1;
	sha256_context sha256; // 224 and 256
	sha512_context sha512; // 384 and 512

} hash_state;

struct NGHex
{
	const char *n_hex;
	const char *g_hex;
};

/* All constants here were pulled from Appendix A of RFC 5054 */
static struct NGHex global_Ng_constants[] = {
#ifdef SRP_NG_1024
	{ /* 1024 */
		"EEAF0AB9ADB38DD69C33F80AFA8FC5E86072618775FF3C0B9EA2314C"
		"9C256576D674DF7496EA81D3383B4813D692C6E0E0D5D8E250B98BE4"
		"8E495C1D6089DAD15DC7D7B46154D6B6CE8EF4AD69B15D4982559B29"
		"7BCF1885C529F566660E57EC68EDBC3C05726CC02FD4CBF4976EAA9A"
		"FD5138FE8376435B9FC61D2FC0EB06E3",
		"2"
	},
#endif
#ifdef SRP_NG_1536
	{ /* 1536 */
		"9DEF3CAFB939277AB1F12A8617A47BBBDBA51DF499AC4C80BEEEA961"
		"4B19CC4D5F4F5F556E27CBDE51C6A94BE4607A291558903BA0D0F843"
		"80B655BB9A22E8DCDF028A7CEC67F0D08134B1C8B97989149B609E0B"
		"E3BAB63D47548381DBC5B1FC764E3F4B53DD9DA1158BFD3E2B9C8CF5"
		"6EDF019539349627DB2FD53D24B7C48665772E437D6C7F8CE442734A"
		"F7CCB7AE837C264AE3A9BEB87F8A2FE9B8B5292E5A021FFF5E91479E"
		"8CE7A28C2442C6F315180F93499A234DCF76E3FED135F9BB",
		"2"
	},
#endif
#ifdef SRP_NG_2048
	{ /* 2048 */
		"AC6BDB41324A9A9BF166DE5E1389582FAF72B6651987EE07FC319294"
		"3DB56050A37329CBB4A099ED8193E0757767A13DD52312AB4B03310D"
		"CD7F48A9DA04FD50E8083969EDB767B0CF6095179A163AB3661A05FB"
		"D5FAAAE82918A9962F0B93B855F97993EC975EEAA80D740ADBF4FF74"
		"7359D041D5C33EA71D281E446B14773BCA97B43A23FB801676BD207A"
		"436C6481F1D2B9078717461A5B9D32E688F87748544523B524B0D57D"
		"5EA77A2775D2ECFA032CFBDBF52FB3786160279004E57AE6AF874E73"
		"03CE53299CCC041C7BC308D82A5698F3A8D0C38271AE35F8E9DBFBB6"
		"94B5C803D89F7AE435DE236D525F54759B65E372FCD68EF20FA7111F"
		"9E4AFF73",
		"2"
	},
#endif
#ifdef SRP_NG_3072
	{ /* 3072 */
		"FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E08"
		"8A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B"
		"302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9"
		"A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE6"
		"49286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8"
		"FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D"
		"670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C"
		"180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF695581718"
		"3995497CEA956AE515D2261898FA051015728E5A8AAAC42DAD33170D"
		"04507A33A85521ABDF1CBA64ECFB850458DBEF0A8AEA71575D060C7D"
		"B3970F85A6E1E4C7ABF5AE8CDB0933D71E8C94E04A25619DCEE3D226"
		"1AD2EE6BF12FFA06D98A0864D87602733EC86A64521F2B18177B200C"
		"BBE117577A615D6C770988C0BAD946E208E24FA074E5AB3143DB5BFC"
		"E0FD108E4B82D120A93AD2CAFFFFFFFFFFFFFFFF",
		"5"
	},
#endif
#ifdef SRP_NG_4096
	{ /* 4096 */
		"FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E08"
		"8A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B"
		"302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9"
		"A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE6"
		"49286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8"
		"FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D"
		"670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C"
		"180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF695581718"
		"3995497CEA956AE515D2261898FA051015728E5A8AAAC42DAD33170D"
		"04507A33A85521ABDF1CBA64ECFB850458DBEF0A8AEA71575D060C7D"
		"B3970F85A6E1E4C7ABF5AE8CDB0933D71E8C94E04A25619DCEE3D226"
		"1AD2EE6BF12FFA06D98A0864D87602733EC86A64521F2B18177B200C"
		"BBE117577A615D6C770988C0BAD946E208E24FA074E5AB3143DB5BFC"
		"E0FD108E4B82D120A92108011A723C12A787E6D788719A10BDBA5B26"
		"99C327186AF4E23C1A946834B6150BDA2583E9CA2AD44CE8DBBBC2DB"
		"04DE8EF92E8EFC141FBECAA6287C59474E6BC05D99B2964FA090C3A2"
		"233BA186515BE7ED1F612970CEE2D7AFB81BDD762170481CD0069127"
		"D5B05AA993B4EA988D8FDDC186FFB7DC90A6C08F4DF435C934063199"
		"FFFFFFFFFFFFFFFF",
		"5"
	},
#endif
#ifdef SRP_NG_6144
	{ /* 6144 */
		"FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E08"
		"8A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B"
		"302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9"
		"A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE6"
		"49286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8"
		"FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D"
		"670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C"
		"180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF695581718"
		"3995497CEA956AE515D2261898FA051015728E5A8AAAC42DAD33170D"
		"04507A33A85521ABDF1CBA64ECFB850458DBEF0A8AEA71575D060C7D"
		"B3970F85A6E1E4C7ABF5AE8CDB0933D71E8C94E04A25619DCEE3D226"
		"1AD2EE6BF12FFA06D98A0864D87602733EC86A64521F2B18177B200C"
		"BBE117577A615D6C770988C0BAD946E208E24FA074E5AB3143DB5BFC"
		"E0FD108E4B82D120A92108011A723C12A787E6D788719A10BDBA5B26"
		"99C327186AF4E23C1A946834B6150BDA2583E9CA2AD44CE8DBBBC2DB"
		"04DE8EF92E8EFC141FBECAA6287C59474E6BC05D99B2964FA090C3A2"
		"233BA186515BE7ED1F612970CEE2D7AFB81BDD762170481CD0069127"
		"D5B05AA993B4EA988D8FDDC186FFB7DC90A6C08F4DF435C934028492"
		"36C3FAB4D27C7026C1D4DCB2602646DEC9751E763DBA37BDF8FF9406"
		"AD9E530EE5DB382F413001AEB06A53ED9027D831179727B0865A8918"
		"DA3EDBEBCF9B14ED44CE6CBACED4BB1BDB7F1447E6CC254B33205151"
		"2BD7AF426FB8F401378CD2BF5983CA01C64B92ECF032EA15D1721D03"
		"F482D7CE6E74FEF6D55E702F46980C82B5A84031900B1C9E59E7C97F"
		"BEC7E8F323A97A7E36CC88BE0F1D45B7FF585AC54BD407B22B4154AA"
		"CC8F6D7EBF48E1D814CC5ED20F8037E0A79715EEF29BE32806A1D58B"
		"B7C5DA76F550AA3D8A1FBFF0EB19CCB1A313D55CDA56C9EC2EF29632"
		"387FE8D76E3C0468043E8F663F4860EE12BF2D5B0B7474D6E694F91E"
		"6DCC4024FFFFFFFFFFFFFFFF",
		"5"
	},
#endif
#ifdef SRP_NG_8192
	{ /* 8192 */
		"FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E08"
		"8A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B"
		"302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9"
		"A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE6"
		"49286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8"
		"FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D"
		"670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C"
		"180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF695581718"
		"3995497CEA956AE515D2261898FA051015728E5A8AAAC42DAD33170D"
		"04507A33A85521ABDF1CBA64ECFB850458DBEF0A8AEA71575D060C7D"
		"B3970F85A6E1E4C7ABF5AE8CDB0933D71E8C94E04A25619DCEE3D226"
		"1AD2EE6BF12FFA06D98A0864D87602733EC86A64521F2B18177B200C"
		"BBE117577A615D6C770988C0BAD946E208E24FA074E5AB3143DB5BFC"
		"E0FD108E4B82D120A92108011A723C12A787E6D788719A10BDBA5B26"
		"99C327186AF4E23C1A946834B6150BDA2583E9CA2AD44CE8DBBBC2DB"
		"04DE8EF92E8EFC141FBECAA6287C59474E6BC05D99B2964FA090C3A2"
		"233BA186515BE7ED1F612970CEE2D7AFB81BDD762170481CD0069127"
		"D5B05AA993B4EA988D8FDDC186FFB7DC90A6C08F4DF435C934028492"
		"36C3FAB4D27C7026C1D4DCB2602646DEC9751E763DBA37BDF8FF9406"
		"AD9E530EE5DB382F413001AEB06A53ED9027D831179727B0865A8918"
		"DA3EDBEBCF9B14ED44CE6CBACED4BB1BDB7F1447E6CC254B33205151"
		"2BD7AF426FB8F401378CD2BF5983CA01C64B92ECF032EA15D1721D03"
		"F482D7CE6E74FEF6D55E702F46980C82B5A84031900B1C9E59E7C97F"
		"BEC7E8F323A97A7E36CC88BE0F1D45B7FF585AC54BD407B22B4154AA"
		"CC8F6D7EBF48E1D814CC5ED20F8037E0A79715EEF29BE32806A1D58B"
		"B7C5DA76F550AA3D8A1FBFF0EB19CCB1A313D55CDA56C9EC2EF29632"
		"387FE8D76E3C0468043E8F663F4860EE12BF2D5B0B7474D6E694F91E"
		"6DBE115974A3926F12FEE5E438777CB6A932DF8CD8BEC4D073B931BA"
		"3BC832B68D9DD300741FA7BF8AFC47ED2576F6936BA424663AAB639C"
		"5AE4F5683423B4742BF1C978238F16CBE39D652DE3FDB8BEFC848AD9"
		"22222E04A4037C0713EB57A81A23F0C73473FC646CEA306B4BCBC886"
		"2F8385DDFA9D4B7FA2C087E879683303ED5BDD3A062B3CF5B3A278A6"
		"6D2A13F83F44F82DDF310EE074AB6A364597E899A0255DC164F31CC5"
		"0846851DF9AB48195DED7EA1B1D510BD7EE74D73FAF36BC31ECFA268"
		"359046F4EB879F924009438B481C6CD7889A002ED5EE382BC9190DA6"
		"FC026E479558E4475677E9AA9E3050E2765694DFC81F56E880B96E71"
		"60C980DD98EDD3DFFFFFFFFFFFFFFFFF",
		"13"
	},
#endif
 	{0, 0} /* null sentinel */
};

#ifdef DEBUG
static void
print_mpi(const mpi *m)
{
	unsigned i = 0;
	char buf[8*1024];
	size_t blen = sizeof(buf)/sizeof(buf[0]);

	mpi_write_string(m, 16, buf, &blen);

	while (i < USED(m))
	{
		unsigned n = USED(m) - i;
		n = (n < 16) ? n : 16;
		printf("%04x: ", i);
		for (unsigned j = 0; j < n; ++j)
			printf ("%c%c ", buf[i + 2*j], buf[i + 2*j+1]);
		printf("\n");
		i += n;
	}
}
#endif

static void
mpi_init_multi(mpi *a, ...)
{
	mpi *c = a;
	va_list args;

	va_start(args, a);
	while (c)
	{
		mpi_init(c);
		c = va_arg(args, mpi *);
	}
	va_end(args);
}

static void
mpi_free_multi(mpi *a, ...)
{
	mpi *c = a;
	va_list args;

	va_start(args, a);
	while (c)
	{
		mpi_free(c);
		c = va_arg(args, mpi *);
	}
	va_end(args);
}

static int
f_rng(void *v, unsigned char *c, size_t s)
{
	int n =
		(RAND_MAX) <= INT8_MAX ? 1
		: (RAND_MAX) <= INT16_MAX ? 2
		: 4;

	while (s)
	{
		int l = (s < n) ? s : n;
		s -= l;
		int r = rand();
		while (l--)
		{
			*c++ = r & 0xff;
			r >>= 8;
		}
	}

	return MPI_OK;
}

static int
mpi_rand(mpi *m, size_t s)
{
	mpi_fill_random(m, s, f_rng, NULL);
	return MPI_OK;
}

static bool
init_ng(SrpNgConstant *ng, SrpNgType ng_type, const char *n_hex, const char *g_hex)
{
	if (!ng)
		return false;

	mpi_init_multi(&ng->N, &ng->g, NULL);

	if (ng_type != srp_Ng_CUSTOM)
	{
		n_hex = global_Ng_constants[ng_type].n_hex;
		g_hex = global_Ng_constants[ng_type].g_hex;
	}

	if (!n_hex || !g_hex)
		return false;

	mpi_read_string(&ng->N, 16, n_hex);
	mpi_read_string(&ng->g, 16, g_hex);

	return true;
}

static void
free_ng(SrpNgConstant *ng)
{
	if (!ng)
		return;

	mpi_free(&ng->N);
	mpi_free(&ng->g);
}

static int
hash_init(SrpHashAlgorithm alg, hash_state *c)
{
	switch (alg)
	{
	case srp_SHA1: sha1_init(&c->sha1); sha1_starts(&c->sha1); break;
	case srp_SHA224: sha256_init(&c->sha256); sha256_starts(&c->sha256, 1); break;
	case srp_SHA256: sha256_init(&c->sha256); sha256_starts(&c->sha256, 0); break;
	case srp_SHA384: sha512_init(&c->sha512); sha512_starts(&c->sha512, 1); break;
	case srp_SHA512: sha512_init(&c->sha512); sha512_starts(&c->sha512, 0); break;
	default: return CRYPT_INVALID_HASH;
	}
	return CRYPT_OK;
}

static int
hash_update(SrpHashAlgorithm alg, hash_state *c, const void *data, size_t len)
{
	switch (alg)
	{
	case srp_SHA1: sha1_update(&c->sha1, data, len); break;
	case srp_SHA224:
	case srp_SHA256: sha256_update(&c->sha256, data, len); break;
	case srp_SHA384:
	case srp_SHA512: sha512_update(&c->sha512, data, len); break;
	default: return CRYPT_INVALID_HASH;
	}
	return CRYPT_OK;
}

static int
hash_final(SrpHashAlgorithm alg, hash_state *c, uint8_t *md)
{
	switch (alg)
	{
	case srp_SHA1: sha1_finish(&c->sha1, md); break;
	case srp_SHA224:
	case srp_SHA256: sha256_finish(&c->sha256, md); break;
	case srp_SHA384:
	case srp_SHA512: sha512_finish(&c->sha512, md); break;
	default: return CRYPT_INVALID_HASH;
	}
	return CRYPT_OK;
}

static uint8_t *
hash(SrpHashAlgorithm alg, const uint8_t *d, size_t n, uint8_t *md)
{
	hash_state c;

	if (hash_init(alg, &c) != CRYPT_OK
		|| hash_update(alg, &c, d, n) != CRYPT_OK
		|| hash_final(alg, &c, md) != CRYPT_OK)
		return NULL;
	else
		return md;
}

static int
hash_length(SrpHashAlgorithm alg)
{
	switch (alg)
	{
	case srp_SHA1  : return SHA1_DIGEST_LENGTH;
	case srp_SHA224: return SHA224_DIGEST_LENGTH;
	case srp_SHA256: return SHA256_DIGEST_LENGTH;
	case srp_SHA384: return SHA384_DIGEST_LENGTH;
	case srp_SHA512: return SHA512_DIGEST_LENGTH;
	default: return CRYPT_INVALID_HASH;
	}
}

static bool
H_nn(mpi *r, SrpHashAlgorithm alg, const mpi *n1, const mpi *n2)
{
	uint8_t buff[SHA512_DIGEST_LENGTH];
	int len_n1 = mpi_size(n1);
	int len_n2 = mpi_size(n2);
	int nbytes = len_n1 + len_n2;
	uint8_t *bin = (uint8_t *)malloc(nbytes);

	if (!bin)
		return false;

	if (mpi_write_binary(n1, bin, len_n1) != MPI_OK
		|| mpi_write_binary(n2, bin + len_n1, len_n2) != MPI_OK)
	{
		free(bin);
		return false;
	}

	uint8_t *h = hash(alg, bin, nbytes, buff);
	free(bin);

	if (!h)
		return false;

	return (mpi_read_binary(r, buff, hash_length(alg)) == MPI_OK);
}

static bool
H_ns(mpi *r, SrpHashAlgorithm alg, const mpi *n, const uint8_t *bytes, int len_bytes)
{
	uint8_t buff[SHA512_DIGEST_LENGTH];
	int len_n = mpi_size(n);
	int nbytes = len_n + len_bytes;
	uint8_t *bin = (uint8_t *)malloc(nbytes);

	if (!bin)
		 return false;

	if (mpi_write_binary(n, bin, len_n) != MPI_OK)
	{
		free(bin);
		return false;
	}

	memcpy(bin + len_n, bytes, len_bytes);
	uint8_t *h = hash(alg, bin, nbytes, buff);
	free(bin);

	if (!h)
		return false;

	return (mpi_read_binary(r, buff, hash_length(alg)) == MPI_OK);
}

static bool
calculate_x(mpi *r, SrpHashAlgorithm alg, const mpi *salt, const char *username, const char *password, int password_len)
{
	uint8_t ucp_hash[SHA512_DIGEST_LENGTH];
	hash_state ctx;

	if (hash_init(alg, &ctx) != CRYPT_OK
		|| hash_update(alg, &ctx, username, strlen(username)) != CRYPT_OK
		|| hash_update(alg, &ctx, ":", 1)  != CRYPT_OK
		|| hash_update(alg, &ctx, password, password_len) != CRYPT_OK
		|| hash_final(alg, &ctx, ucp_hash) != CRYPT_OK)
		return false;

	return H_ns(r, alg, salt, ucp_hash, hash_length(alg));
}

static bool
update_hash_n(SrpHashAlgorithm alg, hash_state *ctx, const mpi *n)
{
	unsigned long len = mpi_size(n);
	uint8_t *n_bytes = (uint8_t *)malloc(len);

	if (!n_bytes)
		 return false;

	bool r = mpi_write_binary(n, n_bytes, len) == MPI_OK
		&& hash_update(alg, ctx, n_bytes, len) == CRYPT_OK;

	free(n_bytes);

	return r;
}

static bool
hash_num(SrpHashAlgorithm alg, const mpi *n, uint8_t *dest)
{
	int nbytes = mpi_size(n);
	uint8_t *bin = (uint8_t *)malloc(nbytes);

	if (!bin)
		return false;

	bool r = mpi_write_binary(n, bin, nbytes) == MPI_OK
		&& hash(alg, bin, nbytes, dest) != NULL;

	free(bin);

	return r;
}

static bool
calculate_M(
	SrpHashAlgorithm alg, SrpNgConstant *ng, uint8_t *dest, const char *I,
	const mpi *s, const mpi *A, const mpi *B, const uint8_t *K
)
{
	uint8_t H_N[SHA512_DIGEST_LENGTH];
	uint8_t H_g[SHA512_DIGEST_LENGTH];
	uint8_t H_I[SHA512_DIGEST_LENGTH];
	uint8_t H_xor[SHA512_DIGEST_LENGTH];
	hash_state ctx;
	int hash_len = hash_length(alg);

	if (!hash_num(alg, &ng->N, H_N)
		|| !hash_num(alg, &ng->g, H_g)
		|| hash(alg, (const uint8_t *)I, strlen(I), H_I) == NULL)
		return false;

	for (int i = 0; i < hash_len; ++i)
		H_xor[i] = H_N[i] ^ H_g[i];

	return hash_init(alg, &ctx) == CRYPT_OK
		&& hash_update(alg, &ctx, H_xor, hash_len) == CRYPT_OK
		&& hash_update(alg, &ctx, H_I, hash_len) == CRYPT_OK
		&& update_hash_n(alg, &ctx, s)
		&& update_hash_n(alg, &ctx, A)
		&& update_hash_n(alg, &ctx, B)
		&& hash_update(alg, &ctx, K, hash_len) == CRYPT_OK
		&& hash_final(alg, &ctx, dest) == CRYPT_OK;
}

static bool
calculate_H_AMK(SrpHashAlgorithm alg, uint8_t *dest, const mpi *A, const uint8_t *M, const uint8_t *K)
{
	hash_state ctx;

	return hash_init(alg, &ctx) == CRYPT_OK
		&& update_hash_n(alg, &ctx, A)
		&& hash_update(alg, &ctx, M, hash_length(alg)) == CRYPT_OK
		&& hash_update(alg, &ctx, K, hash_length(alg)) == CRYPT_OK
		&& hash_final(alg, &ctx, dest) == CRYPT_OK;
}

static void
init_random()
{
	if (g_initialized)
		return;

	// Device-dependend random initialization
	g_initialized = srp_random_init();
}

/*
	Exported Functions
*/

void
srp_random_seed(const uint8_t *random_data, int data_length)
{
	g_initialized = true;

	if (random_data)
		switch (data_length)
		{
		case 1: srand(*(uint8_t *)random_data); break;
		case 2: srand(*(uint16_t *)random_data); break;
		default: srand(*(uint32_t *)random_data); break;
		}
		// RAND_seed(random_data, data_length);
}

bool
srp_create_salted_verification_key(
	SrpHashAlgorithm alg, SrpNgType ng_type,
	const char *username, const char *password, int len_password,
	const uint8_t **bytes_s, int *len_s, const uint8_t **bytes_v,
	int *len_v, const char *n_hex, const char *g_hex
)
{
	SrpNgConstant ng;
	mpi s, v, x;
	int l_s, l_v;
	uint8_t *b_v, *b_s;

	if (!init_ng(&ng, ng_type, n_hex, g_hex))
		return false;

	mpi_init_multi(&s, &v, &x, NULL);
	init_random();

	bool r = false;
	// BN_rand(s, 32, -1, 0); // 32-bits, msb can be zero, odd number
	if (mpi_rand(&s, 32) != MPI_OK
		|| !calculate_x(&x, alg, &s, username, password, len_password)
		|| mpi_exp_mod(&v, &ng.g, &x, &ng.N, NULL) != MPI_OK) // v = g^x % N
		goto cleanup_and_exit;

#ifdef DEBUG
	printf("s[%d]:\n", USED(&s));
	print_mpi(&s);
	printf("\n");

	printf("x[%d]:\n", USED(&x));
	print_mpi(&x);
	printf("\n");

	printf("v[%d]:\n", USED(&v));
	print_mpi(&v);
	printf("\n");
#endif

	l_s = mpi_size(&s);
	l_v = mpi_size(&v);

	b_v = (uint8_t *)malloc(l_v);
	b_s = (uint8_t *)malloc(l_s);
	if (b_v == NULL
		|| b_s == NULL
		|| mpi_write_binary(&s, b_s, l_s) != MPI_OK
		|| mpi_write_binary(&v, b_v, l_v) != MPI_OK)
	{
		if (b_v)
			free(b_v);
		if (b_s)
			free(b_s);
		goto cleanup_and_exit;
	}

	if (len_s)
		*len_s = l_s;

	if (len_v)
		*len_v = l_v;

	if (bytes_s)
		*bytes_s = b_s;
	else
		free(b_s);

	if (bytes_v)
		*bytes_v = b_v;
	else
		free(b_v);

	r = true;

cleanup_and_exit:
	mpi_free_multi(&s, &v, &x, NULL);
	free_ng(&ng);

	return r;
}

/* Out: bytes_B, len_B.
 	On failure, bytes_B will be set to NULL and len_B will be set to 0
*/
bool
srp_verifier_init(
	SrpVerifier *ver, SrpHashAlgorithm alg, SrpNgType ng_type, const char *username,
	const uint8_t *bytes_s, int len_s, const uint8_t *bytes_v, int len_v,
	const uint8_t *bytes_A, int len_A, const uint8_t **bytes_B, int *len_B,
	const char *n_hex, const char *g_hex
)
{
	mpi s, v, A, u, B, S, b, k, tmp1, tmp2;
	SrpNgConstant *ng = NULL;
	int ulen;
	
	if (!ver)
		return false;

	mpi_init_multi(&s, &v, &A, &u, &B, &S, &b, &k, &tmp1, &tmp2, NULL);

	bool r = false;
	if (mpi_read_binary(&s, bytes_s, len_s) != MPI_OK
		|| mpi_read_binary(&v, bytes_v, len_v) != MPI_OK
		|| mpi_read_binary(&A, bytes_A, len_A) != MPI_OK)
		goto cleanup_and_exit;

	ulen = strlen(username) + 1;
	ver->username = (char *)malloc(ulen);
	if (!ver->username)
		 goto cleanup_and_exit;
	memcpy(ver->username, username, ulen);

	if (!init_ng(&ver->ng, ng_type, n_hex, g_hex))
		goto cleanup_and_exit;

	ng = &ver->ng;
	init_random();

	ver->hash_alg = alg;
	ver->authenticated = false;

	/* SRP-6a safety check */
	if (mpi_mod_mpi(&tmp1, &A, &ng->N) != MPI_OK)
		goto cleanup_and_exit;

	if (!mpi_iszero(&tmp1))
	{
		// BN_rand(b, 256, -1, 0);
		if (mpi_rand(&b, 256) != MPI_OK
			|| !H_nn(&k, alg, &ng->N, &ng->g)
			/* B = kv + g^b */
			|| mpi_mul_mpi(&tmp1, &k, &v) != MPI_OK
			|| mpi_exp_mod(&tmp2, &ng->g, &b, &ng->N, NULL) != MPI_OK
			|| mpi_add_mpi(&B, &tmp1, &tmp2) != MPI_OK
			|| !H_nn(&u, alg, &A, &B)
			/* S = (A *(v^u)) ^ b */
			|| mpi_exp_mod(&tmp1, &v, &u, &ng->N, NULL) != MPI_OK
			|| mpi_mul_mpi(&tmp2, &A, &tmp1) != MPI_OK
			|| mpi_exp_mod(&S, &tmp2, &b, &ng->N, NULL) != MPI_OK
			|| !hash_num(alg, &S, ver->session_key)
			|| !calculate_M(alg, ng, ver->M, username, &s, &A, &B, ver->session_key)
			|| !calculate_H_AMK(alg, ver->H_AMK, &A, ver->M, ver->session_key))
			goto cleanup_and_exit;

		int l_B = mpi_size(&B);
		uint8_t *b_B = (uint8_t *)malloc(l_B);
		if (!b_B)
			goto cleanup_and_exit;

		if (mpi_write_binary(&B, b_B, l_B) != MPI_OK)
			goto cleanup_and_exit;
		ver->bytes_B = b_B;

		if (len_B)
			*len_B = l_B;

		if (bytes_B)
			*bytes_B = b_B;
	}
	r = true;

cleanup_and_exit:
 	if (!r)
 	{
 		if (ver->username)
			free(ver->username);
		ver->username = NULL;

		if (ng)
			free_ng(ng);
 	}
	mpi_free_multi(&s, &v, &A, &u, &B, &S, &b, &k, &tmp1, &tmp2, NULL);

	return r;
}

void
srp_verifier_clear(SrpVerifier *ver)
{
	if (!ver)
		return;

	free_ng(&ver->ng);
	free(ver->username);
	free(ver->bytes_B);
	memset(ver, 0, sizeof(SrpVerifier));
}

bool
srp_verifier_is_authenticated(SrpVerifier * ver)
{
	return ver->authenticated;
}

const char *
srp_verifier_get_username(SrpVerifier * ver)
{
	return ver->username;
}

const uint8_t *
srp_verifier_get_session_key(SrpVerifier *ver, int *key_length)
{
	if (key_length)
		*key_length = hash_length(ver->hash_alg);
	return ver->session_key;
}

int
srp_verifier_get_session_key_length(SrpVerifier *ver)
{
	return hash_length(ver->hash_alg);
}

/* user_M must be exactly SHA512_DIGEST_LENGTH bytes in size */
bool
srp_verifier_verify_session(SrpVerifier *ver, const uint8_t *user_M, const uint8_t **bytes_HAMK)
{
	bool r = memcmp(ver->M, user_M, hash_length(ver->hash_alg)) == 0;

	if (r)
		ver->authenticated = true;

	if (bytes_HAMK)
		*bytes_HAMK = (r) ? ver->H_AMK : NULL;

	return r;
}

/* */

bool
srp_user_init(
	SrpUser *usr, SrpHashAlgorithm alg, SrpNgType ng_type,
	const char *username, const char *bytes_password, int len_password,
	const char *n_hex, const char *g_hex
)
{
	init_random();

	if (!usr)
		return false;

	if (!init_ng(&usr->ng, ng_type, n_hex, g_hex))
		return false;

	mpi_init_multi(&usr->a, &usr->A, &usr->S, NULL);

	usr->hash_alg = alg;
	int ulen = strlen(username) + 1;
	usr->username = (char *)malloc(ulen);
	usr->password = (char *)malloc(len_password);
	usr->password_len = len_password;

	if (!usr->username || !usr->password)
		 goto err_exit;

	memcpy((char *)usr->username, username, ulen);
	memcpy((char *)usr->password, bytes_password, len_password);

	usr->authenticated = false;
	usr->bytes_A = NULL;

	return true;

 err_exit:
	 if (usr->password)
	 {
		 memset(usr->password, 0, usr->password_len);
		 free(usr->password);
	 }

	 if (usr->username)
	 {
		 memset(usr->username, 0, ulen);
		 free(usr->username);
	 }

	 mpi_free_multi(&usr->a, &usr->A, &usr->S, NULL);
	 free_ng(&usr->ng);

	 return false;
}

void
srp_user_clear(SrpUser *usr)
{
	if (!usr)
		return;

	mpi_free_multi(&usr->a, &usr->A, &usr->S, NULL);
	free_ng(&usr->ng);

	if (usr->username)
		free(usr->username);

	if (usr->password)
	{
		memset(usr->password, 0, usr->password_len);
		free(usr->password);
	}

	if (usr->bytes_A)
		free(usr->bytes_A);

	memset(usr, 0, sizeof(SrpUser));
}

bool
srp_user_is_authenticated(SrpUser *usr)
{
	return usr->authenticated;
}


const char *
srp_user_get_username(SrpUser *usr)
{
	return usr->username;
}

const uint8_t *
srp_user_get_session_key(SrpUser *usr, int *key_length)
{
	if (key_length)
		*key_length = hash_length(usr->hash_alg);

	return usr->session_key;
}

int
srp_user_get_session_key_length(SrpUser *usr)
{
	return hash_length(usr->hash_alg);
}

/* Output: username, bytes_A, len_A */
bool
srp_user_start_authentication(
	SrpUser *usr, const char **username,
	const uint8_t **bytes_A, int *len_A
)
{
	// BN_rand(usr->a, 256, -1, 0);
	if (mpi_rand(&usr->a, 256) != MPI_OK
		|| mpi_exp_mod(&usr->A, &usr->ng.g, &usr->a, &usr->ng.N, NULL) != MPI_OK)
		return false;

	int l_A = mpi_size(&usr->A);
	uint8_t *b_A = (uint8_t *)malloc(l_A);
	if (!b_A)
		return false;

	if (mpi_write_binary(&usr->A, b_A, l_A) != MPI_OK)
	{
		free(b_A);
		return false;
	}

	usr->bytes_A = b_A;

	if (username)
		*username = usr->username;

	if (bytes_A)
		*bytes_A = b_A;

	if (len_A)
		*len_A = l_A;

	return true;
}

/* Output: bytes_M. Buffer length is SHA512_DIGEST_LENGTH */
bool
srp_user_process_challenge(SrpUser * usr,
	const uint8_t *bytes_s, int len_s,
	const uint8_t *bytes_B, int len_B,
	const uint8_t **bytes_M, int *len_M
)
{
	mpi s, B, u, x, k, v, tmp1, tmp2, tmp3;

	mpi_init_multi(&s, &B, &u, &x, &k, &v, &tmp1, &tmp2, &tmp3, NULL);

	bool r = false;
	if (mpi_read_binary(&s, bytes_s, len_s) != MPI_OK
		|| mpi_read_binary(&B, bytes_B, len_B) != MPI_OK
		|| !H_nn(&u, usr->hash_alg, &usr->A, &B)
		|| !calculate_x(&x, usr->hash_alg, &s, usr->username, usr->password, usr->password_len)
		|| !H_nn(&k, usr->hash_alg, &usr->ng.N, &usr->ng.g))
		goto cleanup_and_exit;

	/* SRP-6a safety check */
	if (!mpi_iszero(&B) && !mpi_iszero(&u))
	{
		if (mpi_exp_mod(&v, &usr->ng.g, &x, &usr->ng.N, NULL) != MPI_OK
			/* S = (B - k*(g^x)) ^ (a + ux) */
			|| mpi_mul_mpi(&tmp1, &u, &x) != MPI_OK
			|| mpi_add_mpi(&tmp2, &usr->a, &tmp1) != MPI_OK // tmp2 = (a + ux)
			|| mpi_exp_mod(&tmp1, &usr->ng.g, &x, &usr->ng.N, NULL) != MPI_OK
			|| mpi_mul_mpi(&tmp3, &k, &tmp1) != MPI_OK // tmp3 = k*(g^x)
			|| mpi_sub_mpi(&tmp1, &B, &tmp3) != MPI_OK // tmp1 = (B - K*(g^x))
			|| mpi_exp_mod(&usr->S, &tmp1, &tmp2, &usr->ng.N, NULL) != MPI_OK
			|| !hash_num(usr->hash_alg, &usr->S, usr->session_key)
			|| !calculate_M(usr->hash_alg, &usr->ng, usr->M, usr->username, &s, &usr->A, &B, usr->session_key)
			|| !calculate_H_AMK(usr->hash_alg, usr->H_AMK, &usr->A, usr->M, usr->session_key))
			goto cleanup_and_exit;

		if (bytes_M)
			*bytes_M = usr->M;

		if (len_M)
			*len_M = hash_length(usr->hash_alg);

		r = true;
	}

cleanup_and_exit:
	mpi_free_multi(&s, &B, &u, &x, &k, &v, &tmp1, &tmp2, &tmp3, NULL);

	return r;
}


bool
srp_user_verify_session(SrpUser *usr, const uint8_t *bytes_HAMK)
{
	bool r = memcmp(usr->H_AMK, bytes_HAMK, hash_length(usr->hash_alg)) == 0;

	if (r)
		usr->authenticated = true;

	return r;
}
